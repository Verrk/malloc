/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   page.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/12 14:42:45 by cpestour          #+#    #+#             */
/*   Updated: 2016/03/28 12:51:32 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

t_page		**get_first_page(void)
{
	static t_page	*page = NULL;

	return (&page);
}

void		init_page(t_page *p, size_t p_size, size_t size)
{
	p->type = get_type(size);
	p->size = p_size;
	p->free = 0;
	p->prev = NULL;
	p->next = NULL;
	p->first = NULL;
}

void		add_page(t_page *p)
{
	t_page	*tmp;

	if (*get_first_page() == NULL)
		*get_first_page() = p;
	else
	{
		tmp = *get_first_page();
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = p;
		p->prev = tmp;
	}
}

t_page		*create_new_page(size_t size)
{
	t_page	*new;
	size_t	len;

	len = get_size(size);
	new = (t_page*)mmap(NULL, len, PROT_READ | PROT_WRITE,
						MAP_PRIVATE | MAP_ANON, -1, 0);
	init_page(new, len - PAGE_SIZE, size);
	add_page(new);
	return (new);
}

t_page		*get_page(t_block *b)
{
	t_page	*p;

	p = *get_first_page();
	while ((void*)p < (void*)b)
	{
		if (P_DATA(p) + p->size > (void*)b)
			return (p);
		p = p->next;
	}
	return (NULL);
}
