/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/14 19:13:33 by cpestour          #+#    #+#             */
/*   Updated: 2016/02/02 23:32:40 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		*enlarge_block(t_block *b, size_t size)
{
	size_t	len;
	void	*ptr;

	if (b->next == NULL || b->next->free == 0)
	{
		ptr = malloc(size);
		ft_memcpy(ptr, B_DATA(b), b->size);
		free(B_DATA(b));
		return (ptr);
	}
	else
	{
		len = b->size + b->next->size + BLOCK_SIZE * 2;
		b->size = size;
		split_block(b, len);
		return (B_DATA(b));
	}
}

void		*ft_realloc(t_block *b, size_t size)
{
	size_t	len;

	if (size <= b->size)
	{
		len = b->size + BLOCK_SIZE;
		b->size = size;
		split_block(b, len);
		return (B_DATA(b));
	}
	else
		return (enlarge_block(b, size));
}

void		*realloc(void *ptr, size_t size)
{
	void	*ret;
	t_block	*b;

	if (ptr == NULL)
		return (malloc(size));
	else if (size == 0)
	{
		free(ptr);
		return (malloc(size));
	}
	b = get_block(ptr);
	if (b == NULL)
		return (NULL);
	ret = ft_realloc(b, size);
	return (ret);
}
