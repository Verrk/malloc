/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/11 16:48:36 by cpestour          #+#    #+#             */
/*   Updated: 2016/04/14 16:08:58 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H

# include <unistd.h>
# include <sys/mman.h>

# define BLOCK_SIZE	sizeof(t_block)
# define PAGE_SIZE	sizeof(t_page)

# define TINY_SIZE	(4 * getpagesize())
# define TINY_MAX	128
# define SMALL_SIZE	(32 * getpagesize())
# define SMALL_MAX	1024

# define B_DATA(b)	((void*)((char*)b + sizeof(t_block)))
# define P_DATA(p)	((void*)((char*)p + sizeof(t_page)))

# define ALIGN(s)	((s + getpagesize()) / getpagesize() * getpagesize())

typedef enum		e_type
{
	TINY,
	SMALL,
	LARGE
}					t_type;

struct s_block;

typedef struct		s_page
{
	t_type			type;
	size_t			size;
	int				free;
	struct s_page	*prev;
	struct s_page	*next;
	struct s_block	*first;
}					t_page;

typedef struct		s_block
{
	size_t			size;
	int				free;
	struct s_block	*prev;
	struct s_block	*next;
}					t_block;

void				*malloc(size_t size);
void				free(void *ptr);
void				*realloc(void *ptr, size_t size);
void				*calloc(size_t count, size_t size);
void				show_alloc_mem(void);
t_block				*ft_malloc(size_t size);
void				split_block(t_block *b, size_t len);
t_page				**get_first_page(void);
t_type				get_type(size_t size);
size_t				get_size(size_t size);
void				init_block(t_block *b, size_t size);
t_block				*create_new_block(size_t size);
t_page				*create_new_page(size_t size);
t_block				*get_block(void *ptr);
t_page				*get_page(t_block *b);
void				*ft_memcpy(void *dst, const void *src, size_t size);
void				ft_putaddr(void *ptr);
void				ft_putchar(char c);
void				ft_putstr(const char *str);
void				ft_putnbr(int n);
void				ft_bzero(void *s, size_t n);

#endif
