/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/11 17:04:10 by cpestour          #+#    #+#             */
/*   Updated: 2016/03/28 12:51:44 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	split_block(t_block *b, size_t len)
{
	t_block	*new;

	if (len == b->size + BLOCK_SIZE)
		return ;
	if (len < b->size + BLOCK_SIZE * 2)
	{
		b->size = len - BLOCK_SIZE;
		return ;
	}
	new = (t_block*)(B_DATA(b) + b->size);
	init_block(new, len - b->size - BLOCK_SIZE * 2);
	new->next = b->next;
	b->next = new;
	new->prev = b;
	new->free = 1;
}

t_block	*search_in_page(t_page *p, size_t size)
{
	t_block	*b;

	b = p->first;
	while (b != NULL && !(b->free && b->size >= size))
		b = b->next;
	return (b);
}

t_block	*get_free_block(size_t size)
{
	t_page	*p;
	t_type	t;
	t_block	*b;
	size_t	len;

	b = NULL;
	p = *get_first_page();
	while (p != NULL)
	{
		t = get_type(size);
		if (p->type == t)
			b = search_in_page(p, size);
		if (b != NULL)
			break ;
		p = p->next;
	}
	if (b == NULL)
		return (NULL);
	len = b->size + BLOCK_SIZE;
	b->size = size;
	b->free = 0;
	p->free = 0;
	split_block(b, len);
	return (b);
}

t_block	*ft_malloc(size_t size)
{
	t_block	*b;

	b = get_free_block(size);
	if (b == NULL)
		b = create_new_block(size);
	return (b);
}

void	*malloc(size_t size)
{
	t_block	*b;

	b = ft_malloc(size);
	if (b == NULL)
		return (NULL);
	return (B_DATA(b));
}
