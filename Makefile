#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/09/10 19:49:45 by cpestour          #+#    #+#              #
#    Updated: 2015/09/14 20:42:28 by cpestour         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

CC=gcc
CFLAGS=-Wall -Werror -Wextra -fPIC
SRC=malloc.c block.c page.c type.c free.c realloc.c calloc.c show_alloc_mem.c \
	ft_putaddr.c ft_putchar.c ft_putstr.c ft_putnbr.c ft_memcpy.c ft_bzero.c
OBJ=$(SRC:.c=.o)
NAME=$(shell echo libft_malloc_$(HOSTTYPE).so)

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) -shared -o $@ $^
	ln -fs $@ libft_malloc.so

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

clean:
	rm -f $(OBJ) *~

fclean: clean
	rm -f $(NAME)
	rm -f libft_malloc.so

re: fclean all
