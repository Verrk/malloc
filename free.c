/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/14 06:56:53 by cpestour          #+#    #+#             */
/*   Updated: 2016/02/02 23:31:37 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

t_block		*fusion_block(t_block *b)
{
	t_block	*tmp;

	tmp = b;
	if (b->prev && b->prev->free == 1)
		b = b->prev;
	if (tmp->next && tmp->next->free == 1)
		tmp = tmp->next;
	b->free = 1;
	if (b == tmp)
		return (b);
	b->size = B_DATA(tmp) + tmp->size - B_DATA(b);
	b->next = tmp->next;
	return (b);
}

void		free_page(t_block *b)
{
	t_page	*p;
	t_block	*first;

	p = get_page(b);
	first = p->first;
	if (first->free && first->next == NULL)
		p->free = 1;
}

void		free(void *ptr)
{
	t_block	*b;

	if (ptr == NULL)
		return ;
	b = get_block(ptr);
	if (b == NULL)
		return ;
	b = fusion_block(b);
	free_page(b);
}
