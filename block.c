/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   block.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/14 05:34:32 by cpestour          #+#    #+#             */
/*   Updated: 2016/03/28 12:51:54 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	init_block(t_block *b, size_t size)
{
	if (b == NULL)
		return ;
	b->size = size;
	b->free = 0;
	b->next = NULL;
	b->prev = NULL;
}

t_block	*add_block(t_page *p, size_t size)
{
	t_block	*new;

	new = (t_block*)P_DATA(p);
	init_block(new, size);
	if (p->type != LARGE)
		split_block(new, get_size(size) - PAGE_SIZE);
	p->first = new;
	return (new);
}

t_block	*create_new_block(size_t size)
{
	t_page	*p;
	t_block	*b;

	p = create_new_page(size);
	b = add_block(p, size);
	return (b);
}

t_block	*search_block(t_page *p, void *ptr)
{
	t_block	*b;

	b = p->first;
	while (b != NULL)
	{
		if (B_DATA(b) == ptr)
			return (b);
		b = b->next;
	}
	return (NULL);
}

t_block	*get_block(void *ptr)
{
	t_block	*b;
	t_page	*p;

	p = *get_first_page();
	b = NULL;
	while (p != NULL)
	{
		b = search_block(p, ptr);
		if (b != NULL)
			return (b);
		p = p->next;
	}
	return (NULL);
}
