/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/14 20:14:18 by cpestour          #+#    #+#             */
/*   Updated: 2016/03/28 12:52:02 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		print_type(t_type type)
{
	if (type == TINY)
		ft_putstr("TINY : ");
	else if (type == SMALL)
		ft_putstr("SMALL : ");
	else
		ft_putstr("LARGE : ");
}

int			print_block(t_block *b)
{
	ft_putaddr(B_DATA(b));
	ft_putstr(" - ");
	ft_putaddr(B_DATA(b) + b->size);
	ft_putstr(" : ");
	ft_putnbr(b->size);
	ft_putstr(" octets\n");
	return (b->size);
}

int			print_page(t_page *p)
{
	t_block	*b;
	int		total;

	total = 0;
	print_type(p->type);
	ft_putaddr(P_DATA(p));
	ft_putchar('\n');
	b = p->first;
	while (b != NULL)
	{
		if (b->free == 0)
			total += print_block(b);
		b = b->next;
	}
	return (total);
}

void		show_alloc_mem(void)
{
	t_page	*p;
	int		total;

	total = 0;
	p = *get_first_page();
	if (p == NULL)
		ft_putstr("Nothing allocated\n");
	while (p != NULL)
	{
		if (p->free == 0)
			total += print_page(p);
		p = p->next;
	}
	ft_putstr("Total : ");
	ft_putnbr(total);
	ft_putstr(" octets\n");
}
