/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   type.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/14 05:52:56 by cpestour          #+#    #+#             */
/*   Updated: 2016/02/02 23:28:36 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

t_type		get_type(size_t size)
{
	if (size <= TINY_MAX)
		return (TINY);
	else if (size <= SMALL_MAX)
		return (SMALL);
	else
		return (LARGE);
}

size_t		get_size(size_t size)
{
	if (size <= TINY_MAX)
		return (TINY_SIZE);
	else if (size <= SMALL_MAX)
		return (SMALL_SIZE);
	else
		return (ALIGN(size + PAGE_SIZE + BLOCK_SIZE));
}
